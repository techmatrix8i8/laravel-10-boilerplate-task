FROM php:8.2-fpm-alpine



RUN rm /var/cache/apk/*

WORKDIR /var/www
# COPY . /var/www
# RUN composer i

COPY ./dev/docker-compose/php/supervisord-app.conf /etc/supervisord.conf

ENTRYPOINT ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
